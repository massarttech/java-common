package com.massarttech.common.domain;


import java.io.Serializable;

public abstract class BaseDomain implements Serializable {
    protected String id;
    private   long   time;
    private   long   updateTime;
    private   long   version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BaseDomain) {
            BaseDomain baseDomain = (BaseDomain) o;
            return baseDomain.id != null && baseDomain.id.equals(this.id);
        }
        return false;
    }
}
