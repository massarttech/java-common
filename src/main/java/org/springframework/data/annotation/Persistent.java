package org.springframework.data.annotation;

import org.springframework.stereotype.Indexed;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to generally identify persistent types, fields and parameters.
 *
 * @author Jon Brisbin
 * @author Oliver Gierke
 */
@Indexed
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
public @interface Persistent {
}