package org.springframework.data.mongodb.core.index;

public enum IndexDirection {
    ASCENDING,
    DESCENDING;

    private IndexDirection() {
    }
}